Comentarios sobre el curso de Red Hat
--------------------------------------

Me parecio un curso rapido que engloba bien los temas basicos para manejar sistemas linux desde la terminal, fue una manera dinamica de recordar los comando mas usados en linux como crear, copiar y ekiminar directorios o como usar Vim de manera eficiente, y como en esta materia al parecer usaremos muy seguido la terminal me parecio un curso muy util :)


Adjunto las capturas de pantalla del curso terminado
![Curso terminado](/img/redHat1.png)
![Curso terminado](/img/redHat2.png)
![Curso terminado](/img/redHat3.png)
